//
// BackgroundImageCarousel
//
// Copyright (c) Mario Boikov 2017
// Licensed under the MIT license. See LICENSE file.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var backgroundImageView: UIImageView!

    private let viewModel = ViewModel()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.backgroundImage
            .map(UIImage.init(named:))
            .asDriver(onErrorJustReturn: nil)
            .drive(backgroundImageView.rx.crossDissolveImage)
            .disposed(by: disposeBag)
    }
}
