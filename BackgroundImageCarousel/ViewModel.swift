//
// BackgroundImageCarousel
//
// Copyright (c) Mario Boikov 2017
// Licensed under the MIT license. See LICENSE file.
//

import RxSwift

struct ViewModel {

    static private let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)

    var backgroundImage: Observable<String> = {
        let images = ["Image 1", "Image 2", "Image 3"]

        return Observable<Int>
            .timer(0, period: 5.0, scheduler: ViewModel.backgroundScheduler)
            .map { images[$0 % images.count] }
            .shareReplay(1)
    }()

}
