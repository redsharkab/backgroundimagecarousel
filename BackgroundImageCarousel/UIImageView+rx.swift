//
// BackgroundImageCarousel
//
// Copyright (c) Mario Boikov 2017
// Licensed under the MIT license. See LICENSE file.
//

import RxSwift
import RxCocoa

extension Reactive where Base: UIImageView {

    var crossDissolveImage: UIBindingObserver<Base, UIImage?> {
        return crossDissolveImage(duration: 1.5)
    }

    func crossDissolveImage(duration: TimeInterval) -> UIBindingObserver<Base, UIImage?> {
        return UIBindingObserver(UIElement: base) { imageView, image in
            UIView.transition(
                with: imageView,
                duration: duration,
                options: .transitionCrossDissolve,
                animations: { imageView.image = image },
                completion: nil
            )
        }
    }
}
