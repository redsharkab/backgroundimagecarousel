# Background Image Carousel

Example app of using [RxSwift](http://rxswift.org) to create a background image carousel.

## Acknowledgements
The images used for the example app are licensed under [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed).
   
 * [Image 1](https://pixabay.com/photo-2178464/)
 * [Image 2](https://pixabay.com/photo-2103612/)
 * [Image 3](https://pixabay.com/photo-563800/)

